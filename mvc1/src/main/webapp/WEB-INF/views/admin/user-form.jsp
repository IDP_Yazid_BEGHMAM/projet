<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- intégration des balises  de la biblio spring -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>

<!--  la balise base sert à préfixer l'ensemble  des liens et des images pour éviter des    problèmes de redirection -->
        <base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
	<title>${title}</title>
	<meta charset="utf-8" />
</head>
<body>
       

<h1>
	${title }
</h1>
<form:form method="post" action="admin/save-users" modelAttribute="u">
<form:label path="name">Nom</form:label>
<form:input path="name"/>
<br/>
<form:label path="email">Email</form:label>
<form:input path="email"/>
<br/>
<c:choose>
	<c:when test="${isAdd}">
		<form:label path="password">Mot de passe</form:label>
		<form:password path="password" showPassword="true"/>
	</c:when>
	<c:otherwise>
		<form:hidden path="password"/>
	</c:otherwise>
</c:choose>


<br/>
<form:label path="admin">Admin?</form:label>
<form:checkbox path="admin"/>
<br/>
<form:hidden path="id"/>
<form:hidden path="version"/>
<input type="submit" value="sauvegarder" />

</form:form>
<div>${msg}</div>


</body>
</html>
