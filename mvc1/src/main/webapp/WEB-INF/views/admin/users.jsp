<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>

<!--  la balise base sert à préfixer l'ensemble  des liens et des images pour éviter des    problèmes de redirection -->
        <base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
	
	<title>Home</title>
	<meta charset="utf-8" />
</head>
<body>
<h1>
	liste des utilisateurs 	
</h1>
<a href="admin/add-user"> ajouter utilisateur</a>
 <a href="authenticate">formulaire d'authentification</a>

<br/>

<fieldset>
	<legend>Rechercher par nom</legend>
	<form:form method="post" action="admin/search-user" modelAttribute="u" >
	<form:input path="name"/>
	<input type="submit" value="rechercher" />
	</form:form>
</fieldset>
<c:choose>
<c:when test="${resVide }">
	<p>aucun resultat</p>
</c:when>
<c:otherwise>
<table border="1">
	<tr>
		<th>Nom</th>
		<th>Email</th>
		<th>Admin</th>
		<th>Actions</th>
	</tr>
	<c:forEach var="u" items="${users}">
		<tr> 
			<td>${u.name }</td>
			<td>${u.email }</td>
			<td>
			<c:choose>
			<c:when test="${u.admin }"> oui</c:when>
			<c:otherwise>non</c:otherwise>
			</c:choose>
			</td>
			<td>
			<a href="admin/users/update/${u.id }" title="modifier">modifier</a>
			<a href="admin/users/delete/${u.id }" title="supprimer">supprimer</a>
			</td>
		</tr>
	</c:forEach>
</table>
<c:choose>
 <c:when test="${page>1}">
   <a href="admin/users?page=${page-1}&max=${max}">Précédent</a>
          </c:when>
    <c:otherwise><span>Précédent</span></c:otherwise>
</c:choose>
 <span>${page}</span>
<c:choose>
  <c:when test="${suivExist}">
     <a href="admin/users?page=${page+1}&max=${max}">Suivant</a>
  </c:when>
<c:otherwise>Suivant</c:otherwise>
</c:choose>


<!-- ou en moins bien

<div>
<c:if test="${page>1}"><a href="admin/users?page=${page-1}&max=${max}">precedent</a></c:if>
<span>${page}</span>
<c:if test="${suivExist}"><a href="admin/users?page=${page+1}&max=${max}">suivant</a></c:if>
</div>
-->
</c:otherwise>
</c:choose>

<a href="admin/export-users" title="exporter"> exporter en csv</a>


<h4>import csv</h4>
<form:form method="post" action="admin/upload-users" enctype="multipart/form-data">
	<input type="file" name="file" />
		<br/>
	<input type="submit" value=" uploader" /> 
</form:form>




</body>
</html>