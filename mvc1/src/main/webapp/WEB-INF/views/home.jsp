<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
<head>
<!--  la balise base sert à préfixer l'ensemble  des liens et des images pour éviter des    problèmes de redirection -->
        <base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
	
	<title>Home</title>
	<meta charset="utf-8" />
</head>
<body>
<h1>
	formation spring mvc  
</h1>
<a href="authenticate">formulaire d'authentification</a>

<P>  The time on the server is ${serverTime}. </P>
<br />
        <a href="api/users">WS Liste utilisateurs JSON</a>
        <br />
        <a href="api/users/xml">WS Liste utilisateurs XML</a>
        <br />
        <a href="testBis">Test Data</a>
        <br /><br /><br />
        <a href="contact">contact</a>
</body>
</html>
