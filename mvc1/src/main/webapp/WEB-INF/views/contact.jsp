<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%-- intégration des balises JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- bibliothèques de balises Spring --%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html>
<head>
        <base href="<%=request.getScheme()+"://"+request.getServerName()+":" + request.getServerPort()+request.getContextPath()+"/" %>" />
        
        <title>Authentification</title>
        <meta charset="utf-8" />
</head>
<body>
        <h1>Contactez-nous</h1>
        
        <div style="color:red;">
                <spring:hasBindErrors name="login-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field}" />
                                <c:out value="${err.defaultMessage}" />
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
        
        
        <form:form method="post" action="send" 
                           modelAttribute="contact-form">
                
                <form:label path="name">Votre nom :</form:label>  
                <form:input path="name" />
                <br />           
                <form:label path="email">Email :</form:label>  
                <form:input path="email" />
                <br />
                <form:label path="type">Type :</form:label>  
                <form:select path="type">
                        <form:options items="${typesList}" />
                </form:select>
                <br />
                <form:label path="msg">Msg :</form:label>  
                <form:textarea path="msg" cols="50" rows="4" />
                <br />
                <input type="submit" value="Envoyer" />           
        </form:form>
        <div>${msgRetour}</div>
        
        
</body>
</html>