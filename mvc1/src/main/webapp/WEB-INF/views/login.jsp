<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- intégration des balises  de la biblio spring -->
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>

<!--  la balise base sert à préfixer l'ensemble  des liens et des images pour éviter des    problèmes de redirection -->
<base
	href="<%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ request.getContextPath() + "/"%>" />
<title>Login</title>
<meta charset="utf-8" />
</head>
<body>
	<h1>authentification</h1>
        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=fr">FRANCAIS</a>
        <a href="${requestScope['javax.servlet.forward.request_uri']}?lang=en">ENGLISH</a>
        <div style="color:red;">
                <spring:hasBindErrors name="login-form">
                        <c:forEach var="err" items="${errors.allErrors}">
                                <c:out value="${err.field}" />
                                <c:out value="${err.defaultMessage}" />
                        </c:forEach>
                </spring:hasBindErrors>
        </div>
        
	<form:form method="post" action="check-login"
		modelAttribute="login-form">
		<form:label path="email">
			<spring:message code="login.lblEmail" />
		</form:label>
		<form:input path="email" />
		<br />
		<form:label path="password">
			<spring:message code="login.lblPassword" />
		</form:label>
		<form:password path="password" />
		<br />
		<input type="submit"
			value=" <spring:message code="login.lblsubmit" />" />

	</form:form>
	
	<div>${msg}</div>


</body>
</html>
