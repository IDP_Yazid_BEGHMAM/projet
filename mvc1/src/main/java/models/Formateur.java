package models;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
public class Formateur extends User {

	@OneToOne
	private Module module;
	//@ElementCollection 
	//@CollectionTable(name="module", joinColumns=@JoinColumn (name="idUser"))
	//@MapKeyColumn(name="evaluation")
	//private Map<Module, String> evaluationParStagiaire;

	public Formateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Formateur(int idUser, String nom, String prenom, String email, String password, boolean admin,
			List<OrganismeFormation> organismes, List<Formation> formations, Module module
			) {
		super(idUser, nom, prenom, email, password, admin, organismes, formations);
		this.module = module;
		//this.evaluationParStagiaire = evaluationParStagiaire;
	}

	public Module getModule() {
		return module;
	}

	public void setModule(Module module) {
		this.module = module;
	}

//	public Map<Module, String> getEvaluationParStagiaire() {
//		return evaluationParStagiaire;
//	}
//
//	public void setEvaluationParStagiaire(Map<Module, String> evaluationParStagiaire) {
//		this.evaluationParStagiaire = evaluationParStagiaire;
//	}

	
}
