package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Module")
public class Module implements Comparable<Module> {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idModule;
	
	private String nom;
	private int duree;

    @ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idFormation")
	private Formation formation;

    public Module() {}
    
	public Module(int idModule, String nom, int duree, Formation formation) {
		super();
		this.idModule = idModule;
		this.nom = nom;
		this.duree = duree;
		this.formation = formation;
	}


	public int getIdModule() {
		return idModule;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	@Override
	public int compareTo(Module o) {
		// TODO Auto-generated method stub
		if (this.getIdModule() > o.getIdModule())
			return 1;
		else if (this.getIdModule() < o.getIdModule())
			return -1;
		return 0;
	}
	@Override
	public String toString() {
		return "Module [idModule=" + idModule + ", nom=" + nom + ", duree=" + duree+ "]";
	}
    
    
}
