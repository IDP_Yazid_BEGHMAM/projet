package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="OrganismeFormation")
public class OrganismeFormation implements Comparable<OrganismeFormation> {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idOrganismeFormation;
	
	private String nom;
	
	@ManyToMany
	@JoinColumn(name = "user")
	private List<User> users;
	@ManyToMany
	@JoinColumn(name = "idFormation")
	private List<Formation> formations;

	public OrganismeFormation() {
		// TODO Auto-generated constructor stub
	}

	public OrganismeFormation(int idOrganismeFormation,String nom, List<User> users, List<Formation> formations) {
		super();
		this.idOrganismeFormation = idOrganismeFormation;
		this.nom=nom;
		this.users = users;
		this.formations = formations;
	}

	public List<User> getUsers() {
		return users;
	}

	public void ListUsers(List<User> users) {
		this.users = users;
	}

	public List<Formation> getFormations() {
		return formations;
	}

	public void ListFormations(List<Formation> formations) {
		this.formations = formations;
	}

	public int getIdOrganismeFormation() {
		return idOrganismeFormation;
	}

	public String getNom() {
		return nom;
	}

	public void SetNom(String nom) {
		this.nom = nom;
	}

	@Override
	public int compareTo(OrganismeFormation o) {
		// TODO Auto-generated method stub
		if (this.getIdOrganismeFormation() > o.getIdOrganismeFormation())
			return 1;
		else if (this.getIdOrganismeFormation() < o.getIdOrganismeFormation())
			return -1;
		return 0;
	}

	@Override
	public String toString() {
		return "OrganismeFormation [idOrganismeFormation=" + idOrganismeFormation + ", users=" + users + ", formations="
				+ formations + "]";
	}

}
