package models;

import java.util.Map;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;

@Entity
public class Stagiaire extends User {

//	@ElementCollection 
//	@CollectionTable(name="module", joinColumns=@JoinColumn (name="idUser"))
//	@MapKeyColumn(name="appreciation")
//	private Map<Module, String> appreciations;
//	
//	@ElementCollection 
//	@CollectionTable(name="module", joinColumns=@JoinColumn (name="idUser"))
//	@MapKeyColumn(name="note")
//	private Map<Module, String> notes;
//	
//	@ElementCollection 
//	@CollectionTable(name="module", joinColumns=@JoinColumn (name="idUser"))
//	@MapKeyColumn(name="auto evaluation")
//	private Map<Module, String> autoEvaluation;

	public Stagiaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stagiaire(int idUser, String nom, String prenom, String email, String password, boolean admin,
			List<OrganismeFormation> organismes, List<Formation> formations
			) {
		super(idUser, nom, prenom, email, password, admin, organismes, formations);
//		this.appreciations = appreciations;
//		this.notes = notes;
//		this.autoEvaluation = autoEvaluation;
	}

//	public Map<Module, String> getAppreciations() {
//		return appreciations;
//	}
//
//	public void ListAppreciations(Map<Module, String> appreciations) {
//		this.appreciations = appreciations;
//	}
//
//	public Map<Module, String> getNotes() {
//		return notes;
//	}
//
//	public void ListNotes(Map<Module, String> notes) {
//		this.notes = notes;
//	}
//
//	public Map<Module, String> getAutoEvaluation() {
//		return autoEvaluation;
//	}
//
//	public void ListAutoEvaluation(Map<Module, String> autoEvaluation) {
//		this.autoEvaluation = autoEvaluation;
//	}


}
