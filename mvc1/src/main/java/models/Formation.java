package models;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Formation")
public class Formation implements Comparable<Formation>{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idFormation;
	private String titre;
	private int duree;

    @Temporal(TemporalType.DATE)
    private Date dateDebut;
    
    @Temporal(TemporalType.DATE)
    private Date dateFin;
    
    @ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="formations")
    private OrganismeFormation organisme;

    @ManyToMany(cascade=CascadeType.ALL)
	private List<User> users;

    @OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="idModule")
	private List<Module> modules;

	public Formation() {}

	public Formation(int idFormation, String titre, int duree, Date dateDebut, Date dateFin,
			OrganismeFormation organisme, List<User> users, List<Module> modules) {
		super();
		this.idFormation = idFormation;
		this.titre = titre;
		this.duree = duree;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.organisme = organisme;
		this.users = users;
		this.modules = modules;
	}

	public int getIdFormation() {
		return idFormation;
	}
	
	

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public OrganismeFormation getOrganisme() {
		return organisme;
	}

	public void setOrganisme(OrganismeFormation organisme) {
		this.organisme = organisme;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<Module> getModules() {
		return modules;
	}

	public void setModules(List<Module> modules) {
		this.modules = modules;
	}
	@Override
	public int compareTo(Formation o) {
		// TODO Auto-generated method stub
		if (this.getIdFormation() > o.getIdFormation())
			return 1;
		else if (this.getIdFormation() < o.getIdFormation())
			return -1;
		return 0;
	}

	@Override
	public String toString() {
		return "Formation [idFormation=" + idFormation + ", titre=" + titre + ", duree=" + duree + ", dateDebut="
				+ dateDebut + ", dateFin=" + dateFin+ "]";
	}

	
	

}
