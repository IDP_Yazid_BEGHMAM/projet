package models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="User")
public abstract class User implements Comparable<User> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idUser;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	private boolean admin;

	@ManyToMany
	@JoinColumn(name = "idOrganismeFormation")
	private List<OrganismeFormation> organismes;

	@ManyToMany
	private List<Formation> formations;

	public User() {
		// TODO Auto-generated constructor stub
	}

	public User(int idUser, String nom, String prenom, String email, String password, boolean admin,
			List<OrganismeFormation> organismes, List<Formation> formations) {
		super();
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.password = password;
		this.admin = admin;
		this.organismes = organismes;
		this.formations = formations;
	}

	public int getIdUser() {
		return idUser;
	}

	public String getNom() {
		return nom;
	}

	public void SetNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void SetPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void SetEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void SetPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void SetAdmin(boolean admin) {
		this.admin = admin;
	}

	public List<OrganismeFormation> getOrganismes() {
		return organismes;
	}

	public void SetOrganismes(List<OrganismeFormation> organismes) {
		this.organismes = organismes;
	}

	public List<Formation> getFormations() {
		return formations;
	}

	public void SetFormation(List<Formation> formations) {
		this.formations = formations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (idUser ^ (idUser >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (idUser != other.idUser)
			return false;
		return true;
	}

	@Override
	public int compareTo(User o) {
		// TODO Auto-generated method stub
		if (this.getIdUser() > o.getIdUser())
			return 1;
		else if (this.getIdUser() < o.getIdUser())
			return -1;
		return 0;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", password="
				+ password + ", admin=" + admin + "]";
	}
	
	
	

}
