package fr.dawan.mvc1.formBeans;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class ContactForm {

	@NotEmpty
        private String name;
	@NotEmpty
	@Pattern(regexp="\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b", message="Email invalide")
	    private String email;
        
	@NotEmpty
        public enum DemandType{
                RECLAMATION, QUESTION, AUTRE
        }
	@NotEmpty
        private DemandType type;
	
     @NotEmpty
    @Size(min=30, max=450)     
        private String msg;

   
        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getEmail() {
                return email;
        }

        public void setEmail(String email) {
                this.email = email;
        }

        public DemandType getType() {
                return type;
        }

        public void setType(DemandType type) {
                this.type = type;
        }

        public String getMsg() {
                return msg;
        }

        public void setMsg(String msg) {
                this.msg = msg;
        }
        
        
}
