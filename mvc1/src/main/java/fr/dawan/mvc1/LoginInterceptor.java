package fr.dawan.mvc1;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter{

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
			if(request.getRequestURI().contains("/admin/")) {
				if(request.getSession().getAttribute("email")==null){
					request.getRequestDispatcher("/authenticate").forward(request, response);
					
					
				}
			}
		return true;
	}
	
	
}
