package fr.dawan.mvc1;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import fr.dawan.mvc1.beans.UserOld;
import fr.dawan.mvc1.dao.UserDao;
import fr.dawan.mvc1.tools.Tools;



@Controller
public class AdminController {
	
	@Autowired
	private UserDao userDao;

	
	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@GetMapping("/admin/dashboard")
	//ou @RequestMapping(value="/admin/dashboard", method="RequestMethod.GET)
	public String showDashboard() {
		return "admin/espace-admin";
	}
	
	@GetMapping("/admin/users")	
	public String listUsers(Model m,@RequestParam(name="page",required=false)Integer page, @RequestParam(name="max", required=false)Integer max) {
		
		if(page==null) page=1;
		if(max==null) page=30;
		int start=(page-1)*max;
		List <UserOld> lu= userDao.findAll(start,max);
		m.addAttribute("page",page);
		m.addAttribute("max",max);
		m.addAttribute("suivExist",(page*max)<userDao.nbUsers());

		
		 m.addAttribute("users",lu);// pareil que request.setAttribute
		 m.addAttribute("u",new UserOld());
		 m.addAttribute("isAdd",true);
		 
		 
		 return "admin/users"; //pareil que WEB-INF/views...
	}
	
	
	@GetMapping("/admin/users/{action}/{id}")
	public String update(
            @PathVariable("action") String action,
            @PathVariable("id") long id,
            Model model) {
		String cible="redirect:/admin/users";
		if(action.equals("delete")) {
			userDao.remove(id);
		}else if(action.equals("update")) {
			UserOld u=userDao.findById(id);
			model.addAttribute("u",u);
			model.addAttribute("isAdd",false);
			model.addAttribute("title","modification de l'utilisateur "+u.getId());
			cible="admin/user-form";//WEB-INF/views/admin/user-form.jsp
		}
		return cible;
	}
	
	@PostMapping("/admin/save-users")
	public String saveUser(@ModelAttribute("u") UserOld u, BindingResult result) {
		String cible="redirect:/admin/users";
		if(u.getId()==null ||u.getId()==0) {//insertion
			userDao.insert(u);
		}else {//modif
			userDao.update(u);
		}
		return cible;

	}
	
	@GetMapping("/admin/add-user")
	public String addUser(Model model) {
		model.addAttribute("u",new UserOld());
		model.addAttribute("isAdd",true);
		model.addAttribute("title","Ajout d'un utilisateur");
		
		return "admin/user-form";

	}
	
	@PostMapping("/admin/search-user")	
	public String searchUsers(Model model,@ModelAttribute("u") UserOld u,BindingResult result) {
		List<UserOld> lu=userDao.findByName(u.getName());
		model.addAttribute("resVide",(lu==null||lu.size()==0));
		model.addAttribute("users",lu);
		model.addAttribute("u",u);
		
		
		 return "admin/users"; 
	}
	
	   @GetMapping("/admin/export-users")
       public void exportCsv(HttpServletResponse response) throws Exception {
               response.setContentType("text/csv");
               response.setCharacterEncoding("UTF-8");
               response.setHeader("Content-Disposition", 
                                       "attachment;filename=\"users.csv\"");
               ServletOutputStream out = response.getOutputStream();
               out.write("Name;Email;Admin".getBytes());
               out.write("\n".getBytes());
               for (UserOld u : userDao.findAll()) {
                       StringBuilder ligne = new StringBuilder();
                       ligne.append(u.getName()).append(";");
                       ligne.append(u.getEmail()).append(";");
                       ligne.append(u.isAdmin());
                       ligne.append("\n");
                       out.write(ligne.toString().getBytes());
                       
               }
               out.close();
       }     
	
	   
	   
	   @PostMapping("/admin/upload-users")
	   public String uploadCSV(Model model, HttpServletRequest request, @RequestParam("file") MultipartFile file) throws Exception {
		 if(!file.isEmpty()) {
			 try {
				byte[] contentBytes=file.getBytes();
				 String dirPath = "C:/uploads";
                 //String dirPath = request.getServletContext().getRealPath("")+"/uploads";
                 File dir = new File(dirPath);
                 if(!dir.exists())
                         dir.mkdirs();
                 
                 String filePath = dir.getAbsolutePath()+File.separator+file.getOriginalFilename();
                 try(BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath))){
                      bos.write(contentBytes);   
                 }
                 
                 List<UserOld> myImportedList= Tools.importCsv(filePath);
                 for(UserOld x: myImportedList) {
               	  //A faire: v�rifier si le User existe
               	  userDao.insert(x);
                 }
                 new File(filePath).delete();
                 
         } catch (Exception e) {
                 e.printStackTrace();
         }        
 }
 return "redirect:/admin/users";
}
	   
	   @GetMapping("/admin/disconnect")
	   public String disconnect(HttpServletRequest req) {
		   req.getSession().invalidate();
		   return "redirect:/";
	   }
	   
}
