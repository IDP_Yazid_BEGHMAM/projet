package fr.dawan.mvc1;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.dawan.mvc1.beans.UserOld;
import fr.dawan.mvc1.dao.UserDao;
import fr.dawan.mvc1.formBeans.LoginForm;

@Controller
public class LoginController {
	
	@Autowired //on injecte le bean cr�� au d�marrage
	private UserDao userDao;
	
	
 
	

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}


	@RequestMapping(value="/authenticate",method=RequestMethod.GET)
//ou @GetMapping("/authenticate")
	
	public String showLogin(Model model) {
		//l'objet loginForm sert a initialiser le formulairer et a r�cup les donn�es saisies une fois le formulaire valid�
		LoginForm f=new LoginForm("admin@dawan.fr","admin");
		model.addAttribute("login-form",f);
		return "login"; //WEB-INF/views/login.jsp
	}
	
	
	@PostMapping("/check-login")
	public String checkLogin(
		HttpServletRequest request,
		Model model,//pr passer des params
		@Valid @ModelAttribute("login-form") LoginForm form,//formulaire envoy�
		BindingResult result) {//si erreurs, result sera remplie
		String cible="redirect:/admin/dashboard";
		
		
		if (result.hasErrors()) {
			model.addAttribute("errors", result);
            model.addAttribute("login-form",form);
            return "login";
    		}
		
		
		//check user in database
		UserOld u=userDao.findByEmail(form.getEmail());
		//if (form.getEmail().equals("admin@dawan.fr")&& form.getPassword().equals("admin")) {
		if(u!=null && u.getPassword().equals(form.getPassword())){
					request.getSession().setAttribute("email", form.getEmail());
					request.getSession().setAttribute("user_id", u.getId());
		}else {
			model.addAttribute("msg","erreur d'authentification");
			model.addAttribute("login-form",form);
			cible="login";
		}
		
		return cible;
		
	}
}
