package fr.dawan.mvc1.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.mvc1.beans.UserOld;

public class Tools {
	
	/**
	 * 
	 * 1er commentaire exemple
	 * 
	 * @param filePath
	 * @param users
	 * @throws Exception
	 */
//methode qui exporte en CSV une liste
	//visibilit� mot-cl� typeRetour nomMethode

	public static void toCsv(String filePath, 
            List<UserOld> users) throws Exception {

try(BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))){
bw.write("Id;Name;Email");
bw.newLine();
for (UserOld u : users) {
	bw.write(u.getId()+";"+ u.getName()+";" + u.getEmail());
	bw.newLine();
		}
	}
	}
	
    public static List<UserOld> importCsv(String filePath)
            throws Exception {
List<UserOld> users= new ArrayList<>();
try(BufferedReader reader = new BufferedReader( new FileReader(filePath))){
		reader.readLine();//ligne d'ent�te
		String ligne = null;
		while((ligne=reader.readLine())!=null) {	//si ligne non vide
			if(!ligne.trim().isEmpty()) {
				String [] tab =ligne.split(";");	//d�couper la ligne par rapport au ; (split)
				if(tab.length==3) { 				//si 3 �l�ments dans le tableau
				try {
					UserOld u=new UserOld(); 				//cr�er un User et remplir : name,email, admin
					u.setName(tab[0]);
					u.setEmail(tab[1]);
					u.setAdmin(Boolean.parseBoolean(tab[2]));
					users.add(u); 					//ajouter l'utilisateur cr�� � la liste
				}catch(Exception ex) {
					
				}
			}
			}		
		
			}
		}
return users;
    }  
}
