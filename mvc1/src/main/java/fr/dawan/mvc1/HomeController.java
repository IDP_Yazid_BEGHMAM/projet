package fr.dawan.mvc1;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import application.TestData;
import fr.dawan.mvc1.beans.UserOld;
import fr.dawan.mvc1.beans.UserOld.UserStatus;
import fr.dawan.mvc1.dao.UserDao;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private UserDao userDao;
	
	
	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}



	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
    @GetMapping("/test/insert-data")
    public String insertData() {
    	for (int i=1;i<50;i++) {
            UserOld u = new UserOld(null, "toto"+i, "toto"+i+"@dawan.fr", "toto"+i);
            u.setCreationDate( new Date());
            u.setAdmin(true);
            u.setCurrentStatus(UserStatus.ACTIVE);
            userDao.insert(u);
            userDao.getHibernateTemplate().evict(u);
    	}
            return "home";
    }
	
    @GetMapping("testBis")
    public String testBis() {
    	TestData t = new TestData();
    	t.test();
    	return "home";
    }
    
    
	
}
