package fr.dawan.mvc1.dao;

import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.mvc1.beans.UserOld;


public class UserDao {
	
	private HibernateTemplate hibernateTemplate;


	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}
	
	
	

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}




	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<UserOld> findAll(){				
		return(List<UserOld>)hibernateTemplate.find("FROM User", null);
		//return hibernateTemplate.findByExample(User.class);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<UserOld> findAll(int start, int nb ){
		return 		hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM User").setFirstResult(start).setMaxResults(nb).list();

	}
		
	
	@Transactional
	public Long insert(UserOld u) {
		return (Long) hibernateTemplate.save(u);
		}
	
	@Transactional(readOnly=true)
	public  UserOld findById(Long id) {
	
		return hibernateTemplate.get(UserOld.class, id);
          }
	@Transactional
	public  void update(UserOld u) {
		 hibernateTemplate.saveOrUpdate(u);
	}
	
	@Transactional
	public  void remove(Long id) {
		hibernateTemplate.delete(findById(id));
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public  List<UserOld> findByName(String rech){
		return hibernateTemplate.getSessionFactory()
				.getCurrentSession()
				.createQuery("From User where u.name LIKE :rech")
				.setParameter("rech", "%"+rech+"%")
				.list();
	}
	
	  @Transactional(readOnly=true)
      public long nbUsers() {
              return (Long)hibernateTemplate.find("SELECT COUNT (u.id) FROM User u", null).get(0);
      }
	  
	  @SuppressWarnings("unchecked")
		@Transactional(readOnly=true)
	  public UserOld findByEmail(String email){
          List<UserOld> lu = hibernateTemplate
                  .getSessionFactory()
                  .getCurrentSession()
                  .createQuery("FROM User u WHERE u.email= :email")
                  .setParameter("email", email)
                  .list();        
          if(lu!=null && lu.size()>0)
                  return lu.get(0);
          
          return null;
	  }

}
