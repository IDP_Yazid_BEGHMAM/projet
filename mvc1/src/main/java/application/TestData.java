package application;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;

import dao.*;
import models.*;

public class TestData {


	  @Autowired
	  UserDao userDao;
	  @Autowired
	  FormationDao formationDao;
	  @Autowired
	  ModuleDao moduleDao;
	  @Autowired
	  OrganismeFormationDao orgDao;
	
	  public TestData() {
		  userDao=new UserDao();
		  formationDao=new FormationDao();
		  moduleDao=new ModuleDao();
		  orgDao=new OrganismeFormationDao();
	  }
	
	public void test() {
		System.out.println("ok");
		Set<User> listUsers = new TreeSet<>();

		User admin = new Admin();
		admin.SetNom("toto");
		admin.SetPrenom("tata");
		admin.setPassword("admin");
		admin.setEmail("toto@dawan.fr");
		listUsers.add(admin);

		User stagiaire1 = new Stagiaire();
		stagiaire1.setNom("Dujardin");
		stagiaire1.SetPrenom("Jean");
		stagiaire1.setPassword("p4ssw0rd");
		stagiaire1.setEmail("DujardinJean@dawan.fr");
		listUsers.add(stagiaire1);
		User stagiaire2 = new Stagiaire();
		stagiaire2.setNom("Deschamps");
		stagiaire2.SetPrenom("Alfred");
		stagiaire2.setPassword("p4ssw0rd");
		stagiaire2.setEmail("DeschampsAlfred@dawan.fr");
		listUsers.add(stagiaire2);

		User formateur = new Formateur();
		formateur.setNom("Menu");
		formateur.SetPrenom("Stephane");
		formateur.setPassword("p4ssw0rdMenu");
		formateur.setEmail("MenuStephane@dawan.fr");
		listUsers.add(formateur);

		Set<Module> listModules = new TreeSet<Module>();
		Module java = new Module();
		java.setNom("JAVA SE");
		java.setDuree(15);
		listModules.add(java);

		Module spring = new Module();
		spring.setNom("SPRING");
		spring.setDuree(7);
		listModules.add(spring);

		OrganismeFormation organisme = new OrganismeFormation();
		organisme.setUsers(listUsers);
		Set<OrganismeFormation> organismes = new TreeSet<>();
		organismes.add(organisme);

		Formation formation = new Formation();
		formation.setTitre("Dawan POE JAVA/JEE");
		formation.setDateDebut(new Date());
		formation.setDateFin(new Date());
		formation.setDuree(120);
		formation.setUsers(listUsers);
		formation.setModules(listModules);
		Set<Formation> formations = new TreeSet<>();
		formations.add(formation);

		organisme.setFormations(formations);
		admin.setOrganismes(organismes);
		
		listUsers.stream().forEach(x->x.setOrganismes(organismes));
		listUsers.stream().forEach(x->x.setFormation(formations));
		
		userDao.insert(admin);
		userDao.insert(stagiaire1);
		userDao.insert(stagiaire2);
		userDao.insert(formateur);

		formationDao.insert(formation);

		moduleDao.insert(java);
		moduleDao.insert(spring);

		orgDao.insert(organisme);

		System.out.println("fin normale de l'application TEST***");

	}
}
