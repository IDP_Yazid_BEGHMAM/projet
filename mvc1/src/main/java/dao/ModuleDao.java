package dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import models.Module;


public class ModuleDao {
	private HibernateTemplate hibernateTemplate;
	private List<Module> modules;

	public ModuleDao() {
		hibernateTemplate=new HibernateTemplate();
	}
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setListe(List<Module> pmodules) {
		modules = pmodules;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Module> findAll() {
		return (List<Module>) hibernateTemplate.find("From Module", null);
		// return hibernateTemplate.findByExample(Module.class);
	}

	@Transactional
	public Long insert(Module m) {
		return (Long) hibernateTemplate.save(m);
	}

	@Transactional(readOnly = true)
	public Module findById(long id) {
		return hibernateTemplate.get(Module.class, id);
	}

	@Transactional
	public void update(Module m) {
		hibernateTemplate.saveOrUpdate(m);
	}

	@Transactional
	public void remove(long id) {
		hibernateTemplate.delete(findById(id));
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Module> findByName(String rech) {
		return hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery("FROM Module m WHERE m.nom LIKE :rech") // like = contains
				.setParameter("rech", "%" + rech + "%") // %x = commence par
				.list(); // x% = se finit par
							// %x%=contient
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Module> findAll(int start, int nb) {
		return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM Module").setFirstResult(start)
				.setMaxResults(nb).list();
	}

	@Transactional(readOnly = true)
	public long nbModules() {
		return (Long) hibernateTemplate.find("SELECT COUNT (m.id) FROM Module m", null).get(0);
	}
/*
	public void test() {
		
		for (Long i = nbModules(); i < nbModules()+50; i++) {
			System.out.println(i);
			Module m = new Module();
			m.setName("test" + i);
			m.setEmail("test" + i + "@dawan.fr");
			m.setPassword("test" + i);
			m.setCreationDate(new Date());
			m.setAdmin(true);
			m.setCurrentStatus(ModuleStatus.ACTIVE);
			insert(m);

			hibernateTemplate.evict(m);
			// a chaque fois qu'on stocke un objet via hibernate, celui-ci le garde en
			// cache.
			// la méthode evict permet de supprimer l'objet du cache pour remédier aux
			// problèmes notoires d'hibernate en terme de performance
		}

	}*/

}
