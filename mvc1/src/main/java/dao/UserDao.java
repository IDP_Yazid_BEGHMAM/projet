package dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import models.User;

public class UserDao {
	private HibernateTemplate hibernateTemplate;
	private List<User> users;

	public UserDao() {
		hibernateTemplate=new HibernateTemplate();
	}
	
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setListe(List<User> pusers) {
		users = pusers;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) hibernateTemplate.find("From User", null);
		// return hibernateTemplate.findByExample(User.class);
	}

	@Transactional
	public Long insert(User u) {
		return (Long) hibernateTemplate.save(u);
	}

	@Transactional(readOnly = true)
	public User findById(long id) {
		return hibernateTemplate.get(User.class, id);
	}

	@Transactional
	public void update(User u) {
		hibernateTemplate.saveOrUpdate(u);
	}

	@Transactional
	public void remove(long id) {
		hibernateTemplate.delete(findById(id));
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findByName(String rech) {
		return hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery("FROM User u WHERE u.nom LIKE :rech") // like = contains
				.setParameter("rech", "%" + rech + "%") // %x = commence par
				.list(); // x% = se finit par
							// %x%=contient
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public User findByEmail(String email) {
		List<User> liste = hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery("FROM User u WHERE u.email LIKE :email") // like = contains
				.setParameter("email", "%" + email + "%") // %x = commence par
				.list(); // x% = se finit par
							// %x%=contient
		if (liste != null && liste.size() > 0)
			return liste.get(0);
		else
			return null;

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> findAll(int start, int nb) {
		return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM User").setFirstResult(start)
				.setMaxResults(nb).list();
	}

	@Transactional(readOnly = true)
	public long nbUsers() {
		return (Long) hibernateTemplate.find("SELECT COUNT (u.id) FROM User u", null).get(0);
	}
/*
	public void test() {
		
		for (Long i = nbUsers(); i < nbUsers()+50; i++) {
			System.out.println(i);
			User u = new User();
			u.setName("test" + i);
			u.setEmail("test" + i + "@dawan.fr");
			u.setPassword("test" + i);
			u.setCreationDate(new Date());
			u.setAdmin(true);
			u.setCurrentStatus(UserStatus.ACTIVE);
			insert(u);

			hibernateTemplate.evict(u);
			// a chaque fois qu'on stocke un objet via hibernate, celui-ci le garde en
			// cache.
			// la méthode evict permet de supprimer l'objet du cache pour remédier aux
			// problèmes notoires d'hibernate en terme de performance
		}

	}*/

}
