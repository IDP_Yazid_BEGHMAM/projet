package dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import models.OrganismeFormation;


public class OrganismeFormationDao {
	private HibernateTemplate hibernateTemplate;
	private List<OrganismeFormation> organismeFormations;

	public OrganismeFormationDao() {
		hibernateTemplate=new HibernateTemplate();
	}
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setListe(List<OrganismeFormation> pOrganismeFormations) {
		organismeFormations = pOrganismeFormations;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<OrganismeFormation> findAll() {
		return (List<OrganismeFormation>) hibernateTemplate.find("From OrganismeFormation", null);
		// return hibernateTemplate.findByExample(OrganismeFormation.class);
	}

	@Transactional
	public Long insert(OrganismeFormation o) {
		return (Long) hibernateTemplate.save(o);
	}

	@Transactional(readOnly = true)
	public OrganismeFormation findById(long id) {
		return hibernateTemplate.get(OrganismeFormation.class, id);
	}

	@Transactional
	public void update(OrganismeFormation o) {
		hibernateTemplate.saveOrUpdate(o);
	}

	@Transactional
	public void remove(long id) {
		hibernateTemplate.delete(findById(id));
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<OrganismeFormation> findByName(String rech) {
		return hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery("FROM OrganismeFormation o WHERE o.nom LIKE :rech") // like = contains
				.setParameter("rech", "%" + rech + "%") // %x = commence par
				.list(); // x% = se finit par
							// %x%=contient
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<OrganismeFormation> findAll(int start, int nb) {
		return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM OrganismeFormation").setFirstResult(start)
				.setMaxResults(nb).list();
	}

	@Transactional(readOnly = true)
	public long nbOrganismeFormations() {
		return (Long) hibernateTemplate.find("SELECT COUNT (o.id) FROM OrganismeFormation o", null).get(0);
	}
/*
	public void test() {
		
		for (Long i = nbOrganismeFormations(); i < nbOrganismeFormations()+50; i++) {
			System.out.println(i);
			OrganismeFormation o = new OrganismeFormation();
			o.setName("test" + i);
			o.setEmail("test" + i + "@dawan.fr");
			o.setPassword("test" + i);
			o.setCreationDate(new Date());
			o.setAdmin(true);
			o.setCurrentStatus(OrganismeFormationStatus.ACTIVE);
			insert(o);

			hibernateTemplate.evict(o);
			// a chaque fois qu'on stocke un objet via hibernate, celui-ci le garde en
			// cache.
			// la méthode evict permet de supprimer l'objet du cache pour remédier aux
			// problèmes notoires d'hibernate en terme de performance
		}

	}*/

}
