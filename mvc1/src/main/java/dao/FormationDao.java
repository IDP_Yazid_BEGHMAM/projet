package dao;

import java.util.Date;
import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.transaction.annotation.Transactional;

import models.Formation;



public class FormationDao {
	private HibernateTemplate hibernateTemplate;
	private List<Formation> Formations;


	public FormationDao() {
		hibernateTemplate=new HibernateTemplate();
	}
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}

	public void setListe(List<Formation> pFormations) {
		Formations = pFormations;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Formation> findAll() {
		return (List<Formation>) hibernateTemplate.find("From Formation", null);
		// return hibernateTemplate.findByExample(Formation.class);
	}

	@Transactional
	public Long insert(Formation f) {
		return (Long) hibernateTemplate.save(f);
	}

	@Transactional(readOnly = true)
	public Formation findById(long id) {
		return hibernateTemplate.get(Formation.class, id);
	}

	@Transactional
	public void update(Formation f) {
		hibernateTemplate.saveOrUpdate(f);
	}

	@Transactional
	public void remove(long id) {
		hibernateTemplate.delete(findById(id));
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Formation> findByTitle(String rech) {
		return hibernateTemplate.getSessionFactory().getCurrentSession()
				.createQuery("FROM Formation f WHERE f.titre LIKE :rech") // like = contains
				.setParameter("rech", "%" + rech + "%") // %x = commence par
				.list(); // x% = se finit par
							// %x%=contient
	}


	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Formation> findAll(int start, int nb) {
		return hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("FROM Formation").setFirstResult(start)
				.setMaxResults(nb).list();
	}

	@Transactional(readOnly = true)
	public long nbFormations() {
		return (Long) hibernateTemplate.find("SELECT COUNT (f.id) FROM Formation f", null).get(0);
	}
/*
	public void test() {
		
		for (Long i = nbFormations(); i < nbFormations()+50; i++) {
			System.out.println(i);
			Formation f = new Formation();
			f.setName("test" + i);
			f.setEmail("test" + i + "@dawan.fr");
			f.setPassword("test" + i);
			f.setCreationDate(new Date());
			f.setAdmin(true);
			f.setCurrentStatus(FormationStatus.ACTIVE);
			insert(f);

			hibernateTemplate.evict(f);
			// a chaque fois qu'on stocke un objet via hibernate, celui-ci le garde en
			// cache.
			// la méthode evict permet de supprimer l'objet du cache pour remédier aux
			// problèmes notoires d'hibernate en terme de performance
		}

	}*/

}
